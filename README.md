# Role vars

kubernetes_public_ip_cluster: "x.x.x.x"  
kubernetes_private_ip_cluster: "x.x.x.x" => si les ports du cluster ne sont pas ouverts, utiliser l'ip privée du cluster  

kubernetes_public_ip_worker: "x.x.x.x"  => variable optionnelle pour déployer un worker supplémentaire   
kubernetes_private_ip_worker: "x.x.x.x"  

kubernetes_force_configure: true => forcer l'execution de la commande rke up (default false)  
